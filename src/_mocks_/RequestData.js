
const mockData = {"buttons":[7,9,-41,-47],"bars":[63,67],"limit":140}

export default function request() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
              return resolve(mockData);
            }, 10);
    });
}