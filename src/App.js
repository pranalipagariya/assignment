import React from "react";
import "./App.css";
import ProgressBar from '../src/component/ProgressBar';
class App extends React.Component {
  render() {
    return (
      <ProgressBar />
    );
  }
}
export default App;
