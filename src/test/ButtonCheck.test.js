import React from 'react';
import expect from 'expect';
import { mount, configure} from 'enzyme';
import ProgressBar from '../component/ProgressBar';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

it("check if number of buttons created correct", () => {
    const wrapper = mount(<ProgressBar/>);
    wrapper.setState({buttons : [10,20,30]});
    expect(wrapper.find('.btn').length).toBe(3);
});