import React from 'react';
import expect from 'expect';
import {configure ,mount} from 'enzyme';
import Progress from '../component/ProgressBar';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
describe('Check progress bar functionality', () => {

  test("Check if button 1 click increase/decement the progress value", () => {
    let dummyBars = [];
    dummyBars.push({ count : 1, value : 20, color: "success" });
    dummyBars.push({ count : 2, value : 70, color: "success" });

    const wrapper = mount(<Progress/>);
    wrapper.setState({ buttons: [10,20] , bars : dummyBars});
    wrapper.find('.btn').at(0).simulate('click');
    var progressBar = wrapper.find('.progress-bar').getElements();
    expect(progressBar[0].props.children).toEqual(30);
              
  });

   test("Check if beyond limit then change progress bar color", () => {
    let dummyBars = [];
    dummyBars.push({ count : 1, value : 80, color: "success" });
    dummyBars.push({ count : 2, value : 70, color: "success" });

    const wrapper = mount(<Progress/>);
    wrapper.setState({ buttons: [50,20] , bars : dummyBars , limit : 100});
    wrapper.find('.btn').at(0).simulate('click');
    expect(wrapper.find('.bg-danger').exists()).toBeTruthy()
              
  }); 
});