import React from 'react';
import expect from 'expect';
import { mount, configure} from 'enzyme';
import ProgressBar from '../component/ProgressBar';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

it("check if number of progress bars created correct", () => {
    const wrapper = mount(<ProgressBar/>);
    wrapper.setState({bars : [10,20,30]});
    expect(wrapper.find('.progress-bar').length).toBe(3);
});