import React from 'react';
import expect from 'expect';
import { shallow, configure} from 'enzyme';
import ProgressBar from '../component/ProgressBar';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

it("should render my component", () => {
    const component = shallow(<ProgressBar />);
    expect(component.getElements()).toMatchSnapshot();
});
