
const urls = require('../URLConstants').default;

export const getProgressData = (callback) => {
    fetch(urls.urls.getRequestProgressData)
    .then(res => res.json())
    .then(
      (result) => {
          callback(result);
      },
      (error) => {
         // console.log('Could not fetch data', error);
      }
    )
}