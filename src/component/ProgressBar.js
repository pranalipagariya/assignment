import React from "react";
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { getProgressData } from '../api/Progress';
import '../styles/style.css';

class Assignment extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            buttons: [],
            bars: [],
            limit: 0,
            selectedProgressValue: 1,
            loading : false
        }
    }

    componentDidMount() {
        // API call to fetch the data to show the progress bars.
        this.setState({loading : true})
        getProgressData(this.endpointData);
    }

    /**
     * Callback method to handle the API response.
     */
    endpointData = (response) => {
        this.setState({loading : false})
        this.buttons = [];
        if (response != null) {
            var count = 1;
            response.bars.map(value => {
                
                var object = { count, value, color: "success" , percent:(value/response.limit)*100 };
                count= count +1;
                this.buttons.push(object);
                return 1; 
            })
            this.setState({ buttons: response.buttons, bars: this.buttons, limit: response.limit });
        }
    }

    /**
     * Called on click of buttons to increment/decrement the value of the progress.
     */
    onButtonClick = (e) => {
        var data = [];
        data = this.state.bars;
        this.state.bars.find((o, i) => {
            // eslint-disable-next-line
            if (o.count == this.state.selectedProgressValue) {
                var total = this.state.bars[i].value + parseInt(e.target.innerHTML,10);
                var percent = (((total)/this.state.limit))*100;
                if (percent === 0 || percent < 0) {
                   data[i] = { count: this.state.selectedProgressValue, value: 0 , percent: 0};
                } else {
                    data[i] = { count: this.state.selectedProgressValue, value: total , percent: percent};
                }
                (percent > 100) ?data[i].color = "danger" : data[i].color = "success";
                return true;
            }
            return false;
        });
        this.setState({bars : data});
    }

    /**
     * Called on Dropdown selection.
     */
    handleSelectChange = (e) => {
        this.setState({ selectedProgressValue: e.target.value })
    }
    render() {
        const {loading} = this.state;
        return (
            <div>
                <Card className="card">
                    <h1><center>Progress Bar Demos</center> </h1>
                    {loading ? <Spinner style ={{marginLeft:'50%'}} animation="border" variant="primary" /> : null}
                    {this.state.bars.map((percent) => (
                        <div key = {percent.value}>
                                <ProgressBar now={percent.percent} label={(percent.percent).toFixed(0)+"% ("+percent.value+")"} variant={percent.color} style={{height: '30px', width: '90%', marginLeft: '5%' }} />
                            <br />
                        </div>
                    ))}
                    <br />
                    <div className="selectButtons">
                        <div className="formControl">
                            <Form.Group controlId="progressBarSelect">
                                <Form.Control as="select" defaultValue={"Progress 1"} onChange={this.handleSelectChange}>
                                    {this.state.bars.map(percent => (
                                        <option key = {percent.count} value={percent.count}>#Progress {percent.count}</option>
                                    ))}
                                </Form.Control>
                            </Form.Group>

                        </div>
                        <div id = "button" className="container" key = "button">
                            {this.state.buttons.map(value => (
                                <Button key = {value} onClick={(e) => this.onButtonClick(e)} variant="info" style={{ width: 'max-content', marginLeft: '2%' }}>{value > 0 ? '+' + value : value}</Button>
                            ))}
                        </div>
                    </div>
                    <br />
                </Card>
               
            </div>
        );
    }
}

export default Assignment;